###########################################################################################
#
#                             Image pour l'application SyGAL.
#
###########################################################################################

ARG PHP_VERSION

FROM registre.unicaen.fr:5000/unicaen-dev-php${PHP_VERSION}-apache
#FROM unicaen-dev-php${PHP_VERSION}-apache

LABEL maintainer="Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>"

ENV IMAGEMAGICK_CONF_DIR="/etc/ImageMagick-6"
ENV SAXONC_INSTALL_DIR="/opt/Saxonica/SaxonHEC"
ENV SAXONC_TRANSFORM_CMD_DEPLOY_DIR="/usr/bin"
ENV FPM_PHP_LOG_FILE=/var/log/php-fpm.log

## Installation de packages requis.
RUN apt-get update -qq && \
    apt-get install -y \
        postgresql-client

# Saxon/C transform command (https://www.saxonica.com/download/c.xml)
RUN wget https://www.saxonica.com/download/libsaxon-HEC-setup64-v11.4.zip -P /tmp/ && \
    unzip -o /tmp/libsaxon-HEC-setup64-v11.4.zip -d /tmp/ && \
    mkdir -p ${SAXONC_INSTALL_DIR} && cp -r /tmp/libsaxon-HEC-11.4/* ${SAXONC_INSTALL_DIR}/ && \
    cd ${SAXONC_INSTALL_DIR} && \
    cp *.so /usr/lib/. && cp -r rt /usr/lib/. && cp -r saxon-data /usr/lib/. && \
    export SAXONC_HOME=/usr/lib && \
    cd command && ./buildhec-command.sh && ln -s ${SAXONC_INSTALL_DIR}/command/transform ${SAXONC_TRANSFORM_CMD_DEPLOY_DIR}

# ImageMagick : modif config pour éviter l'erreur "attempt to perform an operation not allowed by the security policy `PDF'"
RUN sed -i '/<policy domain="coder" rights="none" pattern="PDF"/d' ${IMAGEMAGICK_CONF_DIR}/policy.xml

# qpdf
RUN apt-get -y install qpdf

# Nettoyage.
RUN apt-get autoremove -y && apt-get clean && rm -rf /tmp/* /var/tmp/*

# Symlink apache access and error logs to stdout/stderr so Docker logs shows them.
RUN ln -sf /dev/stdout /var/log/apache2/access.log
RUN ln -sf /dev/stdout /var/log/apache2/other_vhosts_access.log
RUN ln -sf /dev/stderr /var/log/apache2/error.log

# Configuration Apache.
ADD config/apache/apache-ports.conf     ${APACHE_CONF_DIR}/ports.conf
ADD config/apache/apache-site.conf      ${APACHE_CONF_DIR}/sites-available/app.conf
ADD config/apache/apache-site-ssl.conf  ${APACHE_CONF_DIR}/sites-available/app-ssl.conf
RUN a2ensite app app-ssl

# Configuration PHP, php-fpm.
ADD config/php/fpm/pool.d/www.conf.part /tmp/
RUN cat /tmp/www.conf.part >> ${PHP_CONF_DIR}/fpm/pool.d/www.conf && rm /tmp/www.conf.part
ADD config/php/fpm/conf.d/99-sygal.ini ${PHP_CONF_DIR}/fpm/conf.d/
ADD config/php/cli/conf.d/99-sygal.ini ${PHP_CONF_DIR}/cli/conf.d/

# Copie des scripts complémentaires à lancer au démarrage du container.
COPY entrypoint.d/* /entrypoint.d/

# Création du fichier pour les logs FPM (cf. fpm/pool.d/www.conf.part)
RUN touch ${FPM_PHP_LOG_FILE} && \
    chown www-data:www-data ${FPM_PHP_LOG_FILE}

## Liquibase (+ Java)
#ADD resources/jre-8u321-linux-x64.tar.gz /opt/java/
#ENV JAVA_HOME="/opt/java/jre1.8.0_321"
#ADD resources/liquibase-4.8.0.tar.gz /opt/liquibase/
#ENV PATH="/opt/liquibase/:${PATH}"