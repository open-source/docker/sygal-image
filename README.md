# Image Docker pour l'application SyGAL

## Obtention des sources de l'image 

```bash
git clone https://git.unicaen.fr/open-source/docker/sygal-image.git
cd sygal-image
```

## Construction de l'image (build)

Construisez l'image pour la version de PHP désirée... 

Exemple pour PHP 8.0 :
```bash
PHP_VERSION=8.0 ; \
docker build \
--rm \
--build-arg PHP_VERSION=${PHP_VERSION} \
-t sygal-image-php${PHP_VERSION} \
.
```

**Attention !** Si vous êtes derrière un proxy, vous pouvez transmettre les variables d'environement `*_PROXY` 
à la commande `build` avec des `--build-arg` additionnels, exemple :

```bash
PHP_VERSION=8.0 ; \
docker build \
--rm \
--build-arg PHP_VERSION=${PHP_VERSION} \
--build-arg http_proxy="http://proxy.unicaen.fr:3128" \
--build-arg https_proxy="http://proxy.unicaen.fr:3128" \
--build-arg no_proxy="localhost,.unicaen.fr" \
-t sygal-image-php${PHP_VERSION} \
.
```

## Utilisation dans un `docker-compose.yml`

```
version: '2.2'

services:
  sygal:
    image: sygal-image-php7.3
    container_name: sygal-container-php7.3
    environment:
      - http_proxy
      - https_proxy
      - no_proxy
    ports:
      - "8003:443"
    volumes:
     - .:/app
    working_dir: /app
```

## Utilisation en ligne de commande

*Pré-requis : se placer dans le répertoire contenant les sources de SyGAL.*

- Exemple : démarrage ponctuel d'un container pour lancer un `composer install`

```bash
PHP_VERSION=8.0 ; \
docker run \
--rm \
-it \
--volume ${PWD}:/app \
--workdir /app \
sygal-image-php${PHP_VERSION} \
composer install
```

- Lancement d'une commande dans un container déjà démarré

```bash
docker exec \
sygal-container-php8.0 \
php -v
```
