#!/usr/bin/env bash

#
# Script d'install d'un serveur, traduction du Dockerfile.
#

usage() {
  cat << EOF
Script d'install d'un serveur accueillant l'application SyGAL, traduction du Dockerfile.
Usage: $0 <version de PHP>
EOF
  exit 0;
}

[[ -z "$1" ]] && usage

################################################################################################################

CURR_DIR=$(cd `dirname $0` && pwd)

PHP_VERSION="$1"
FPM_PHP_LOG_FILE=/var/log/fpm-php.www.log
SAXONC_INSTALL_DIR="/opt/Saxonica/SaxonHEC"
SAXONC_TRANSFORM_CMD_DEPLOY_DIR="/usr/bin"

set -e

# Minimum vital
apt-get -qq update && \
apt-get install -y \
    git \
    nano \
    ghostscript-x \
    php${PHP_VERSION}-imagick \
    imagemagick \
    postgresql-client

# Récupération de l'image Docker Unicaen et lancement de son Dockerfile.sh.
# Variables d'env exportées par le Dockerfile.sh de l'image Unicaen :
#   APACHE_CONF_DIR=/etc/apache2
#   PHP_CONF_DIR="/etc/php/$1"
export UNICAEN_IMAGE_TMP_DIR=/tmp/docker-unicaen-image
git clone https://git.unicaen.fr/open-source/docker/unicaen-image.git ${UNICAEN_IMAGE_TMP_DIR}
cd ${UNICAEN_IMAGE_TMP_DIR}
. Dockerfile.sh ${PHP_VERSION}

# Saxon/C PHP module (https://www.saxonica.com/saxon-c/documentation11/index.html#!starting/installingphp)
wget https://www.saxonica.com/download/libsaxon-HEC-setup64-v11.4.zip -P /tmp/ && \
    unzip -o /tmp/libsaxon-HEC-setup64-v11.4.zip -d /tmp/ && \
    mkdir -p ${SAXONC_INSTALL_DIR} && cp -r /tmp/libsaxon-HEC-11.4/* ${SAXONC_INSTALL_DIR}/ && \
    cd ${SAXONC_INSTALL_DIR} && \
    cp *.so /usr/lib/. && cp -r rt /usr/lib/. && cp -r saxon-data /usr/lib/. && \
    export SAXONC_HOME=/usr/lib && \
    cd command && ./buildhec-command.sh && ln -s ${SAXONC_INSTALL_DIR}/command/transform ${SAXONC_TRANSFORM_CMD_DEPLOY_DIR} && \
    cd ${CURR_DIR}

# qpdf
apt-get -y install qpdf

# Passage à Composer v2
composer self-update --2

# Configuration Apache.
cp config/apache/apache-ports.conf     ${APACHE_CONF_DIR}/ports.conf
cp config/apache/apache-site.conf      ${APACHE_CONF_DIR}/sites-available/app.conf
cp config/apache/apache-site-ssl.conf  ${APACHE_CONF_DIR}/sites-available/app-ssl.conf
a2ensite app app-ssl

# Configuration PHP, php-fpm.
cp config/php/fpm/pool.d/www.conf.part /tmp/
cat /tmp/www.conf.part >> ${PHP_CONF_DIR}/fpm/pool.d/www.conf && rm /tmp/www.conf.part
cp config/php/fpm/conf.d/99-sygal.ini ${PHP_CONF_DIR}/fpm/conf.d/
cp config/php/cli/conf.d/99-sygal.ini ${PHP_CONF_DIR}/cli/conf.d/

# Activation du mode de fonctionnement 'production'.
sed -i -re 's/SetEnv APPLICATION_ENV "(development|test)"/SetEnv APPLICATION_ENV "production"/' \
    ${APACHE_CONF_DIR}/sites-available/app-ssl.conf

# Rechargement de services.
service apache2 reload && \
service php${PHP_VERSION}-fpm reload
